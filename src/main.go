package main

import (
	"log"
	"net/http"
	"encoding/json"
	"encoding/base64"
	"math/rand"
	"database/sql"
	_ "github.com/lib/pq"
	"os"
	"fmt"
	"time"
)

type writeRequest struct {
	// !Important! For the json package to properly decode from json in go, fields must be exported (capitalized) within the struct definition.
	ExpireMinutes int `json:"expiration_length_in_minutes"`
	Content string `json:"paste_contents"`
}

type readRequest struct {
	ShortlinkId string `json:"shortlink"`
}

func generate_unique_shortlink_id() string {
	unique := true// default false
	
	var base64_encoded_data string
	// regenrate and check again until result is unique
	// what to in situation when all unique ids will finish?
	for unique {
		var err error

		base64_encoded_data = base64_random_string()
		// check that generated id doesn't exist in the DB
		

		// chech if shortlink_id already exist
		
		var is_shortlink_exist bool

		is_shortlink_exist_query := fmt.Sprintf(`
		SELECT
			CASE WHEN EXISTS 
			(
				SELECT * FROM %s WHERE shortlink = '%s'
			)
			THEN 'TRUE'
			ELSE 'FALSE'
		END`, table_name, base64_encoded_data)

		err = db.QueryRow(is_shortlink_exist_query).Scan(&is_shortlink_exist)
		if err != nil {
			log.Fatal(err)
		}
		// if shortlink id is not present - break the loop and pass the unqiue value
		if ! is_shortlink_exist {
			break
		}
	}
	return base64_encoded_data

}

func base64_random_string() string {
	const letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
	length := 15
	random_string := ""
	for i := 0; i < length; i++ {
		random_string += string(letters[rand.Intn(len(letters))])
	}
	base64_result_string := (base64.StdEncoding.EncodeToString([]byte(random_string)))[:7]
	return base64_result_string
}

func launchServer() {
	initDB()
	// process write request on /api/v1/write endpoint
	http.HandleFunc("/api/v1/write", writeURL)
	// process read request on /api/v1/read endpoint
	http.HandleFunc("/api/v1/read", readURL)
	// start a web server on port 9999 without default handler
	log.Fatal(http.ListenAndServe(":9999", nil))
	

}

func readURL(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		// 405 error, method is not allowed
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
	}

	var jsonBody readRequest
	err := json.NewDecoder(r.Body).Decode(&jsonBody)
	if err != nil {
		// 400 error, bad request
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var content string
	var expiration_length_in_minutes time.Duration
	var created_at time.Time
	shortlink_read_query := fmt.Sprintf(`SELECT content,created_at,expiration_length_in_minutes FROM %s WHERE shortlink = '%s'`,table_name, jsonBody.ShortlinkId)
	err = db.QueryRow(shortlink_read_query).Scan(&content, &created_at, &expiration_length_in_minutes)
	if err != nil {
		if err == sql.ErrNoRows {
			response := fmt.Sprintf("Shortlink with id '%s' doesn't exist or expired.",jsonBody.ShortlinkId)
			// Return 404, not found
			http.Error(w, response, http.StatusNotFound)
			return
			// there were no rows, but otherwise no error occurred
		} else {
			log.Fatal(err)
		}
	}

	response := fmt.Sprintf(`
	{
		"content": %s,
		"created_at": %s,
		"expire_at": %s
	}`, 
	content, 
	created_at.Format(time.RFC3339), // just tranlate time into RFC3339 format
	created_at.Add(time.Minute * expiration_length_in_minutes).Format(time.RFC3339)) // add expiration_length_in_minutes to the created time and format to time.RFC3339

	w.Write([]byte(response))

}
// http.ResponseWriter value assembles the HTTP server's response
// http.Request is a data structure that represents the client HTTP request
func writeURL(w http.ResponseWriter, r *http.Request) {
	// only post requests allowed
	if r.Method != "POST" {
		// 405 error, method is not allowed
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
	}
	// processing client request
	var jsonBody writeRequest
	err := json.NewDecoder(r.Body).Decode(&jsonBody)
	if err != nil {
		// 400 error, bad request
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// generate shortlink id
	unique_shortlink_id := generate_unique_shortlink_id()
	// defer is_shortlink_exist.Close()
	// write shortlink to the database
	new_shortlink_query := fmt.Sprintf(`
	INSERT INTO %s (shortlink,expiration_length_in_minutes,created_at,content) values ('%s','%d','%s','%s');`,
	table_name, unique_shortlink_id, jsonBody.ExpireMinutes, time.Now().Format(time.RFC3339) ,jsonBody.Content)
	_, err = db.Exec(new_shortlink_query)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Generated shortlink: %s",unique_shortlink_id)
	// response to the client
	client_response := `{"shortlink":"` + unique_shortlink_id + `"}`
	w.Write([]byte(client_response))





	
}

func initDB() {
	var err error
	db_host := os.Getenv("DB_ADDRESS")
	db_port := os.Getenv("DB_PORT")
	db_user := os.Getenv("DB_USER") 
	db_password := os.Getenv("DB_PASSWORD")
	db_name := os.Getenv("DB_NAME")
	table_name = os.Getenv("TABLE_NAME")
	connection_string := fmt.Sprintf("user=%s dbname=%s host=%s port=%s password=%s sslmode=disable", db_user, db_name, db_host, db_port, db_password) 
	// sql.Open returns a connection pool not a signle connection. New connection will be created based on request
	db, err = sql.Open("postgres", connection_string)
	if err != nil {
		panic(err)
	}
	// defer db.Close()
    // check that db is working
	err = db.Ping()
	if err != nil {
		panic(err)
	}
	log.Printf("Successfully connected to the database by address: %s:%s.", db_host, db_port)
}





var db *sql.DB // global variable that will be used by by handlers after initDB function
var table_name string // name of the table. used in multiple places


func main() {
	launchServer()
}
